# Exploring Parameter Spaces with Artificial Intelligence and Machine Learning Black-Box Optimisation Algorithms


# Abstract

Constraining Beyond the Standard Model theories usually involves scanning highly multi-dimensional parameter spaces and check observable predictions against experimental bounds and theoretical constraints. Such task is often timely and computationally expensive, especially when the model is severely constrained and thus leading to very low random sampling efficiency. In this work we tackled this challenge using Artificial Intelligence and Machine Learning search algorithms used for Black-Box optimisation problems. Using the cMSSM and the pMSSM parameter spaces, we consider both the Higgs mass and the Dark Matter Relic Density constraints to study their sampling efficiency and parameter space coverage. We find our methodology to produce orders of magnitude improvement of sampling efficiency whilst reasonably covering the parameter space.

# Requirements

- `SPheno 4.0.5` installation
- `MicrOMEGAS 5.2.13` installation, `MSSM` compiled for low-scale `spc` files evaluation.
- A `python 3.10+` environment, python packages installed as `pip install -r requirements.txt`. 

# Running

- `run_all.sh` will run all samplers for `cMSSM` and `pMSSM`, with and without `MicrOMEGAS`. In this file the following options exist for the run:
  - `CPUCOUNTS`: How many CPUs to use, i.e. parallel jobs.
  - `MAXEPISODES`: How many episodes. A single run is an episode.
- `spawn-jobs.sh` parallelises all the jobs for a single combination of `MSSM` variant and `MicrOMEGAS` flag. This runs all samplers.
- `job.sh` runs a single job, with a single sampler, for a single combination of `MSSM` variant and `MicrOMEGAS` flag.

# Metrics and Analysis

The following scripts are provided to produce the episode metrics and plot the respective plots:

- `analysis-metrics-calc.py`: computes a collection of metrics, both per-episode and grouped. Outputs the following files:
  - `full_episode_metrics.csv`: Contains efficiency, mean euclidean distances, wassterstein distances, trial time for each episode.
  - `trial_evolution.csv`: Contains trial metrics for trial evolution within the episodes.
  - `grouped_episode_metrics.csv`: Contains the grouped metrics from `full_episode_metrics.csv` to produce the scan statistics tables.

The above metrics are used in the following scripts to produce plots:

- `analysis-time-plots.py`: Produces trial evolution time plot.
- `analysis-metrics-plots.py`: Produces the metrics plots.
- `analysis-physics-scatter-plots.py`: Produces the scatter plots.
- `analysis-physics-distributions-plots.py`: Produces parameter distributions plots.

# Other Configuration

- In `defaults.yaml` set the paths to `SPheno` and `MicrOMEGAS`, as well as the `n_trials` per episode.


# Released under MIT Licence

Copyright 2022 by the authors

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
