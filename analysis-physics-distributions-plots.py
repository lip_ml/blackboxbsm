# %%
import os

import matplotlib.pyplot as plt
import mplhep as hep
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.signal import find_peaks
from utils.analysis import *

# %%
# Preamble
sns.set_theme("paper")
sns.set(font_scale=font_scale)
run_name = "run_fixed"
path_plots = os.path.join(run_name, "plots")
os.makedirs(path_plots, exist_ok=True)


# %%
# Data
store = pd.HDFStore(os.path.join(run_name, "AllResults.h5"))


for key in store.keys():

    results = store[key]  # .sample(1000)
    results["MicrOMEGAS"] = results["MicrOMEGAS"].apply(lambda x: x == "True")
    model = results["Model"].iloc[0]
    micromegas = results["MicrOMEGAS"].iloc[0]
    print("Starting with model {} results for MicrOMEGAS={}".format(model, micromegas))

    results["physical"] = (results["value"] < np.inf).astype(int)
    samplers = ["random", "tpe", "nsgaii", "cmaes"]
    results["Sampler"] = pd.Categorical(results["Sampler"].tolist(), categories=samplers)

    f, ax = plt.subplots(1, 1, figsize=figsize_square_individual)
    ax.set_title(
        r"{}, {}".format(
            model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
        )
    )
    bins = 10
    for sampler in samplers:
        data = results.query(f"Sampler == '{sampler}' ")["value"].values
        data = np.log(data[(data < np.inf) & (data > 0)] + 1)

        h, bins = np.histogram(data, bins=bins, density=True)

        hep.histplot(h, bins, ax=ax, label=nice_sampler_names[sampler])
    ax.set_ylabel("Density (a.u.)")
    ax.set_xlabel("log(Loss+1)")
    ax.legend(loc="best")
    plt.savefig(os.path.join(path_plots, f"{model}_mo_{micromegas}_loss.pdf"), bbox_inches="tight")
    plt.clf()
    plt.cla()

    for param in all_params:
        if param not in results.columns:
            continue
        if "phase_mu" in param:
            continue
        bins = 25
        hs = {}
        fig, axs = plt.subplots(
            2, 1, gridspec_kw={"height_ratios": [3, 1]}, figsize=figsize_square_individual
        )

        for sampler in samplers:
            h, bins = np.histogram(
                results.query(f"user_attrs_valid_point == True & Sampler == '{sampler}' ")[
                    param
                ].values/1000,
                bins=bins,
                density=True,
            )
            hs[sampler] = h
            hep.histplot(
                h,
                bins,
                label=nice_sampler_names[sampler],
                ax=axs[0],
            )
            axs[0].set_title(
                r"{}, {}".format(
                    model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
                )
            )
            
            #axs[0].set_ylim(0,3*max(h))
            axs[0].set_ylim(0,1.5*max(h))
            axs[0].set_ylabel("Density (a.u.)")
            axs[0].set_xlim(min(bins), max(bins))
            #axs[0].legend(loc="best")
    
            hep.histplot(
                h / (hs["random"] + np.finfo(float).eps),
                bins,
                label=nice_sampler_names[sampler],
                ax=axs[1],
            )
            
            axs[1].set_ylabel("Sampler/Random")
            axs[1].set_xlabel(nice_params_names[param])
            axs[1].set_xlim(min(bins), max(bins))
            #axs[1].set_ylim(0.1, 4)
            axs[1].set_ylim(0.1, 2)
            #axs[1].set_yticks([0.1,1,2,3])
            axs[1].set_yticks([0.1,1,2])
            plt.subplots_adjust(wspace = 0.5, hspace = 0.5)
        
        plt.savefig(
            os.path.join(path_plots, f"{model}_mo_{micromegas}_{param.split('PAR_')[-1]}.pdf"),
            bbox_inches="tight",
        )
        fig.canvas.flush_events()
        plt.clf()
        plt.cla()

    bins = 10
    hs = {}
    fig, axs = plt.subplots(
        2, 1, gridspec_kw={"height_ratios": [3, 1]}, figsize=figsize_square_individual
    )
    for sampler in samplers:
        h, bins = np.histogram(
            results.query(f"user_attrs_valid_point == True  & Sampler == '{sampler}' ")[
                "user_attrs_m_h0"
            ].values,
            bins=bins,
            density=True,
        )
        hs[sampler] = h
        hep.histplot(
            h,
            bins,
            label=nice_sampler_names[sampler],
            ax=axs[0],
        )
        axs[0].set_title(
            r"{}, {}".format(
                model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
            )
        )
        #axs[0].set_ylim(0,3*max(h))
        axs[0].set_ylim(0,1.5*max(h))
        axs[0].set_ylabel("Density (a.u.)")
        axs[0].set_xlim(min(bins), max(bins))
        #axs[0].legend(loc="best")
        hep.histplot(
            h / (hs["random"] + np.finfo(float).eps),
            bins,
            label=nice_sampler_names[sampler],
            ax=axs[1],
        )
        axs[1].set_ylabel("Sampler/Random")
        axs[1].set_xlim(min(bins), max(bins))
        #axs[1].set_ylim(0.1, 4)
        axs[1].set_ylim(0.1, 2)
        #axs[1].set_yticks([0.1,1,2,3])
        axs[1].set_yticks([0.1,1,2])
        axs[1].set_xlabel(r"$m_{h^0}$ (GeV)")
        plt.subplots_adjust(wspace = 0.5, hspace = 0.5)
    
    plt.savefig(os.path.join(path_plots, f"{model}_mo_{micromegas}_mh0.pdf"), bbox_inches="tight")
    plt.clf()
    plt.cla()

    if micromegas == True:
        bins = 10
        hs = {}
        fig, axs = plt.subplots(
            2, 1, gridspec_kw={"height_ratios": [3, 1]}, figsize=figsize_square_individual
        )
        for sampler in samplers:
            h, bins = np.histogram(
                results.query(f"user_attrs_valid_point == True & Sampler == '{sampler}' ")[
                    "user_attrs_omega"
                ].values,
                bins=bins,
                density=True,
            )
            hs[sampler] = h
            hep.histplot(
                h,
                bins,
                label=nice_sampler_names[sampler],
                ax=axs[0],
            )
            axs[0].set_title(
                r"{}, {}".format(
                    model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
                )
            )
            #axs[0].set_ylim(0,3*max(h))
            axs[0].set_ylim(0,1.5*max(h))
            axs[0].set_ylabel("Density (a.u.)")
            axs[0].set_xlim(min(bins), max(bins))
            #axs[0].legend(loc="best")
            hep.histplot(
                h / (hs["random"] + np.finfo(float).eps),
                bins,
                label=nice_sampler_names[sampler],
                ax=axs[1],
            )
            axs[1].set_ylabel("Sampler/Random")
            axs[1].set_xlim(min(bins), max(bins))
            axs[1].set_ylim(0.1, 2)
            axs[1].set_yticks([0.1,1,2])
            #axs[1].set_ylim(0.1, 4)
            #axs[1].set_yticks([0.1,1,2,3])
            axs[1].set_xlabel(r"$\Omega_{DM}h^2$")
            plt.subplots_adjust(wspace = 0.5, hspace = 0.5)

        plt.savefig(
            os.path.join(path_plots, f"{model}_mo_{micromegas}_omega.pdf"), bbox_inches="tight"
        )
        plt.clf()
        plt.cla()
