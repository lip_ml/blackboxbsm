# %%
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from utils.analysis import *

# %%
# Preamble
sns.set_theme("paper")
sns.set(font_scale=font_scale)
run_name = "run_fixed"
path_plots = os.path.join(run_name, "plots")
os.makedirs(path_plots, exist_ok=True)


samplers_to_colors = {
    "random": sns.color_palette()[0],
    "tpe": sns.color_palette()[1],
    "nsgaii": sns.color_palette()[2],
    "cmaes": sns.color_palette()[3],
}

# %%
# Data
store = pd.HDFStore(os.path.join(run_name, "AllResults.h5"))

for key in store.keys():
    results = store[key]
    results["mt"] = np.sqrt(results['user_attrs_m_~t_1'] * results['user_attrs_m_~t_2'])
    results["MicrOMEGAS"] = results["MicrOMEGAS"] == "True"
    model = results["Model"].iloc[0]
    micromegas = results["MicrOMEGAS"].iloc[0]
    print("Starting with model {} results for MicrOMEGAS={}".format(model, micromegas))
    results["Sampler"] = pd.Categorical(results["Sampler"].tolist(), categories=samplers)

    f, axs = plt.subplots(2, 2, figsize=figsize_square_grid, sharex=True, sharey=True)
    axs = axs.flatten()
    f.suptitle(
        r"{}, {}".format(
            model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
        ),
    )
    for idx, sampler in enumerate(samplers):

        mark_size=0.10
        if micromegas == True and sampler == 'random':
            mark_size = 1.0
        if micromegas == True and sampler == 'tpe':
            mark_size = 0.3

        axs[idx].scatter(
            x=results.query(f"user_attrs_valid_point == True and Sampler == '{sampler}'")[At]/1000,
            y=results.query(f"user_attrs_valid_point == True and Sampler == '{sampler}'")["mt"]/1000,
            s=mark_size,
            color=samplers_to_colors[sampler],
            label=nice_sampler_names[sampler],
            edgecolors="none",
        )
        axs[idx].set_title(nice_sampler_names[sampler])
        if idx % 2 == 0:
            axs[idx].set_ylabel(r"$\tilde m_t$ (TeV)")
        #if idx >= 2:
        axs[idx].set_xlabel(r"$A_t$ (TeV)")

    f.tight_layout()

    plt.savefig(
        os.path.join(path_plots, f"{model}_mo_{micromegas}_At_vs_mt.png"),
        dpi=120,
        bbox_inches="tight",
    )

    if micromegas == True:
        ################################################################################################################
        plt.clf()
        plt.cla()

        f, axs = plt.subplots(2, 2, figsize=figsize_square_grid, sharex=False, sharey=True)
        axs = axs.flatten()
        f.suptitle(
            r"{}, {}".format(
                model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
            ),
        )
        for idx, sampler in enumerate(samplers):

            mark_size = 0.15
            if sampler == 'random':
                mark_size = 0.5
            if sampler == 'tpe':
                mark_size = 0.3

            axs[idx].scatter(
                x=results.query(f"user_attrs_valid_point == True and Sampler == '{sampler}'")[M1]/1000,
                y=results.query(f"user_attrs_valid_point == True and Sampler == '{sampler}'")[M2]/1000,
                s=mark_size,
                color=samplers_to_colors[sampler],
                label=nice_sampler_names[sampler],
                edgecolors="none",
            )
            axs[idx].set_title(nice_sampler_names[sampler])
            if idx % 2 == 0:
                axs[idx].set_ylabel(r"$M_2$ (TeV)")
            #if idx >= 2:
            axs[idx].set_xlabel(r"$M_1$ (TeV)")

        f.tight_layout()

        plt.savefig(
            os.path.join(path_plots, f"{model}_mo_{micromegas}_m1_vs_m2.png"),
            dpi=120,
            bbox_inches="tight",
        )
        ################################################################################################################
        plt.clf()
        plt.cla()

        f, axs = plt.subplots(2, 2, figsize=figsize_square_grid, sharex=False, sharey=True)
        axs = axs.flatten()
        f.suptitle(
            r"{}, {}".format(
                model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
            ),
        )
        for idx, sampler in enumerate(samplers):

            mark_size = 0.15
            if sampler == 'random':
                mark_size = 0.5
            if sampler == 'tpe':
                mark_size = 0.3

            axs[idx].scatter(
                x=results.query(f"user_attrs_valid_point == True and Sampler == '{sampler}'")[M1]/1000,
                y=results.query(f"user_attrs_valid_point == True and Sampler == '{sampler}'")[mu]/1000,
                s=mark_size,
                color=samplers_to_colors[sampler],
                label=nice_sampler_names[sampler],
                edgecolors="none",
            )
            axs[idx].set_title(nice_sampler_names[sampler])
            if idx % 2 == 0:
                axs[idx].set_ylabel(r"$\mu$ (TeV)")
            #if idx >= 2:
            axs[idx].set_xlabel(r"$M_1$ (TeV)")

        f.tight_layout()

        plt.savefig(
            os.path.join(path_plots, f"{model}_mo_{micromegas}_m1_vs_mu.png"),
            dpi=120,
            bbox_inches="tight",
        )
        ################################################################################################################
        plt.clf()
        plt.cla()

        f, axs = plt.subplots(2, 2, figsize=figsize_square_grid, sharex=False, sharey=True)
        axs = axs.flatten()
        f.suptitle(
            r"{}, {}".format(
                model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
            ),
        )
        for idx, sampler in enumerate(samplers):

            mark_size = 0.15
            if sampler == 'random':
                mark_size = 0.5
            if sampler == 'tpe':
                mark_size = 0.3

            axs[idx].scatter(
                x=results.query(f"user_attrs_valid_point == True and Sampler == '{sampler}'")[M2]/1000,
                y=results.query(f"user_attrs_valid_point == True and Sampler == '{sampler}'")[mu]/1000,
                s=mark_size,
                color=samplers_to_colors[sampler],
                label=nice_sampler_names[sampler],
                edgecolors="none",
            )
            axs[idx].set_title(nice_sampler_names[sampler])
            if idx % 2 == 0:
                axs[idx].set_ylabel(r"$\mu$ (TeV)")
            #if idx >= 2:
            axs[idx].set_xlabel(r"$M_2$ (TeV)")

        f.tight_layout()

        plt.savefig(
            os.path.join(path_plots, f"{model}_mo_{micromegas}_m2_vs_mu.png"),
            dpi=120,
            bbox_inches="tight",
        )
        ###############################################################################################################
# %%
