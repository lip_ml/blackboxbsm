# %%
import os

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from utils.analysis import *

# %%
# Preamble
sns.set_theme("paper")
sns.set(font_scale=font_scale)
run_name = "run_fixed"
path_plots = os.path.join(run_name, "plots")
os.makedirs(path_plots, exist_ok=True)


# %%
# Data
store = pd.HDFStore(os.path.join(run_name, "AllResults.h5"))
results = pd.DataFrame()

for key in store.keys():
    results = pd.concat(
        [
            results,
            store[key][
                [
                    "Model",
                    "MicrOMEGAS",
                    "Sampler",
                    "Episode",
                    "number",
                    "datetime_start",
                    "datetime_complete",
                ]
            ],
        ],
        ignore_index=True,
    )
# %%
results["MicrOMEGAS"] = results["MicrOMEGAS"] == "True"
results["datetime_complete"] = pd.to_datetime(results["datetime_complete"])
results["datetime_start"] = pd.to_datetime(results["datetime_start"])
results["trial_time"] = (results["datetime_complete"] - results["datetime_start"]).apply(
    lambda x: x.total_seconds()
)

results["Sampler"] = pd.Categorical(results["Sampler"].tolist(), categories=samplers)
# %%
fig, ax = plt.subplots(
    2,
    2,
    figsize=figsize_square_grid,
    sharex=True,
)
for idx, model in enumerate(["cMSSM", "pMSSM"]):
    for jdx, micromegas in enumerate([False, True]):
        ax[idx, jdx].set_title(
            r"{}, {}".format(
                model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
            )
        )
        sns.lineplot(
            x="number",
            y="trial_time",
            hue="Sampler",
            data=results.query(f"Model == '{model}' and MicrOMEGAS == {micromegas}")
            .reset_index(drop=True)
            .replace(nice_sampler_names),
            ax=ax[idx, jdx],
            ci=95,
        )
        ax[idx, jdx].set_xlabel("")
        ax[idx, jdx].set_ylabel("")
        if idx == 0 and jdx == 0:
            ax[idx, jdx].legend()
        else:
            ax[idx, jdx].get_legend().remove()
        if idx == 1 and jdx == 1:
            ax[idx, jdx].set_ylim(0.035, 0.135)
        if idx == 0 and jdx == 1:
            ax[idx, jdx].set_ylim(0.037, 0.08)
        if idx == 1:
            ax[idx, jdx].set_xlabel("Trial Number")
        if jdx == 0:
            ax[idx, jdx].set_ylabel("Time (s)")

plt.savefig(os.path.join(path_plots, f"trial_time.pdf"), bbox_inches="tight")

# %%
# fig, ax = plt.subplots(2, 2, figsize=figsize_square_grid)
# for idx, model in enumerate(["cMSSM", "pMSSM"]):
#     for jdx, micromegas in enumerate([False, True]):
#         ax[idx, jdx].set_title(
#             r"{}, {}".format(
#                 model, r"$C(m_{h^0} \cap \Omega_{DM} h^2)$" if micromegas else r"$C(m_{h^0})$"
#             )
#         )
#         sns.boxenplot(
#             x="Sampler",
#             y="trial_time",
#             data=results.query(f"Model == '{model}' and MicrOMEGAS == {micromegas}")
#             .reset_index(drop=True)
#             .replace(nice_sampler_names),
#             ax=ax[idx, jdx],
#             showfliers=False,
#         )
#         ax[idx, jdx].set_xlabel("")
#         ax[idx, jdx].set_ylabel("")
#         if jdx == 1:
#             ax[idx, jdx].set_ylim(-0.01, 0.32)
#         if idx == 1:
#             ax[idx, jdx].set_xlabel("Trial Number")
#         if jdx == 0:
#             ax[idx, jdx].set_ylabel("Time (s)")


# plt.savefig(os.path.join(path_plots, f"trial_time_distributions.pdf"), bbox_inches="tight")
