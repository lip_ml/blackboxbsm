# %%
import warnings
warnings.filterwarnings('ignore')
warnings.simplefilter('ignore')
import pandas as pd
import numpy as np
import glob
import os


# %%
MODELS = glob.glob("*/")


if os.path.exists("AllResults.h5"):
    os.remove("AllResults.h5")

for model in MODELS:
    print("Staring with model {}".format(model))
    for mo_subfolder in glob.glob(os.path.join(model, "*")):
        mo = mo_subfolder.split("/")[-1].split("-")[-1]
        print("Starting with MicrOMEGAS {}".format(mo))
        for sampler_subfolder in glob.glob(os.path.join(mo_subfolder, "*")):
            sampler = sampler_subfolder.split("/")[-1]
            print("Starting with sampler {}".format(sampler))
            trial_files = glob.glob(os.path.join(sampler_subfolder, "*.csv"))
            for trial_file in trial_files:
                episode_result = pd.read_csv(trial_file, index_col=0)
                episode_result["Sampler"] = sampler
                episode_result["Episode"] = int(trial_file.split("/")[-1].split("_")[0])
                episode_result["Model"] = model.split("/")[0]
                episode_result["MicrOMEGAS"] = mo
                print("Found {} with {} points.".format(trial_file, episode_result.shape[0]))
                print("Saving to HDF5")
                episode_result = episode_result[[col for col in episode_result.columns if "system_attrs_" not in col]]
                episode_result.to_hdf("AllResults.h5", key=f"{model.split('/')[0]}/mo-{mo}", mode="a", append=True, format="table")
                print("Episode results saved.")


# %%
print("Results saved, all done!")
