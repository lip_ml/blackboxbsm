#!/bin/bash
RUNNAME=$1
source venv/bin/activate
cp collect.py $RUNNAME/collect.py
cd $RUNNAME
python collect.py
exit 0
