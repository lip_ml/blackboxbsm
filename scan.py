# %%
import argparse
import os
import subprocess
import yaml

import numpy as np
import optuna

import xslha

from utils.scan import *

optuna.logging.set_verbosity(optuna.logging.CRITICAL)

defaults = yaml.safe_load(open("defaults.yaml", "r"))

N_TRIALS = defaults["n_trials"]
SPHENO_PATH = defaults["spheno_path"]
MICROMEGAS_PATH = defaults["micromegas_path"]
EPISODE_TIMEOUT = defaults["episode_timeout"]

parser = argparse.ArgumentParser(
    description="scan",
    formatter_class=argparse.RawTextHelpFormatter,
)

parser.add_argument(
    "-m",
    "--model",
    action="store",
    dest="model",
    type=str,
    nargs="?",
    default="cMSSM",
    help="Model. Default: %(default)s",
)

parser.add_argument(
    "-s",
    "--sampler",
    action="store",
    dest="sampler",
    type=str,
    nargs="?",
    default="random",
    help="Model. Default: %(default)s",
)

parser.add_argument(
    "-mo",
    "--micromegas",
    action="store",
    dest="micromegas",
    type=str,
    nargs="?",
    default="False",
    help="Model. Default: %(default)s",
)


parser.add_argument(
    "-ep",
    "--episode",
    action="store",
    dest="episode",
    type=int,
    nargs="?",
    default=1,
    help="Model. Default: %(default)s",
)

args = parser.parse_args()
MODEL = args.model
SAMPLER = args.sampler
MICROMEGAS = args.micromegas == "True"
EPISODE = args.episode

if MODEL not in ["cMSSM", "pMSSM"]:
    raise ValueError("Model argument needs to be one of {}".format(["cMSSM", "pMSSM"]))

if SAMPLER not in ["random", "tpe", "cmaes", "nsgaii"]:
    raise ValueError(
        "Model argument needs to be one of {}".format(["random", "tpe", "cmaes", "nsgaii"])
    )

SAMPLERS = {
    "random": optuna.samplers.RandomSampler,
    "tpe": optuna.samplers.TPESampler,
    "cmaes": optuna.samplers.CmaEsSampler,
    "nsgaii": optuna.samplers.NSGAIISampler,
}

SAMPLERS_CONFIG = {
    "random": {},
    "tpe": {"multivariate": True},
    "cmaes": {"restart_strategy": "ipop"},
    "nsgaii": {},
}


# %%
def objective(trial):
    "Runs objective function for a choice of model"

    trial_number = trial.number

    if MODEL == "cMSSM":

        m0 = trial.suggest_float("m0", 0, 1)
        m0 = 10000 * m0
        m12 = trial.suggest_float("m12", 0, 1)
        m12 = 10000 * m12
        A0 = trial.suggest_float("A0", 0, 1)
        A0 = A0 * 12 - 6
        A0 = A0 * m0
        tanb = trial.suggest_float("tanb", 0, 1)
        tanb = tanb * 48.5 + 1.5
        out_blocks = {
            "MODSEL": {"1": 1},
            "SMINPUTS": {
                "2": 1.166379e-05,
                "3": 1.184000e-01,
                "4": 9.118760e01,
                "5": 4.180000e00,
                "6": 1.731000e02,
                "7": 1.776820e00,
            },
            "MINPAR": {"1": m0, "2": m12, "4": 1, "5": A0},
            "EXTPAR": {"25": tanb},
            "SPhenoInput": {"  1": -1, "  2": 0, " 11": 0, " 12": 1.000e-04, " 21": 0, " 49": 0},
        }

    elif MODEL == "pMSSM":
        M10 = trial.suggest_float("M1", 0, 1)
        M10 = M10 * 2 - 1
        M20 = trial.suggest_float("M2", 0, 1)
        M20 = M20 * 2 - 1
        M30 = trial.suggest_float("M3", 0, 1)
        mu0 = trial.suggest_float("mu", 0, 1)
        mu0 = mu0 * 2 - 1
        At0 = trial.suggest_float("At", 0, 1)
        At0 = At0 * 2 - 1
        Ab0 = trial.suggest_float("Ab", 0, 1)
        Ab0 = Ab0 * 2 - 1
        Atau0 = trial.suggest_float("Atau", 0, 1)
        Atau0 = Atau0 * 2 - 1
        M_A0 = trial.suggest_float("M_A", 0, 1)
        m_L10 = trial.suggest_float("m_L1", 0, 1)
        m_e10 = trial.suggest_float("m_e1", 0, 1)
        m_L30 = trial.suggest_float("m_L3", 0, 1)
        m_e30 = trial.suggest_float("m_e3", 0, 1)
        m_Q10 = trial.suggest_float("m_Q1", 0, 1)
        m_u10 = trial.suggest_float("m_u1", 0, 1)
        m_d10 = trial.suggest_float("m_d1", 0, 1)
        m_Q30 = trial.suggest_float("m_Q3", 0, 1)
        m_u30 = trial.suggest_float("m_u3", 0, 1)
        m_d30 = trial.suggest_float("m_d3", 0, 1)
        tanb0 = trial.suggest_float("tan1", 0, 1)

        M1 = np.sign(M10) * (np.abs(M10) * 3950 + 50)
        M2 = np.sign(M20) * (np.abs(M20) * 3600 + 400)
        M3 = M30 * 3000 + 1000
        mu = np.sign(mu0) * (np.abs(mu0) * 3600 + 400)
        At = At0 * 6000
        Ab = Ab0 * 4000
        Atau = Atau0 * 4000
        M_A = M_A0 * 3900 + 100
        m_L1 = m_L10 * 3900 + 100
        m_e1 = m_e10 * 3900 + 100
        m_L3 = m_L30 * 3900 + 100
        m_e3 = m_e30 * 3900 + 100
        m_Q1 = m_Q10 * 3300 + 700
        m_u1 = m_u10 * 3300 + 700
        m_d1 = m_d10 * 3300 + 700
        m_Q3 = m_Q30 * 3300 + 700
        m_u3 = m_u30 * 3300 + 700
        m_d3 = m_d30 * 3300 + 700
        tanb = tanb0 * 59 + 1
        m_L2 = m_L1
        m_e2 = m_e1
        m_Q2 = m_Q1
        m_u2 = m_u1
        m_d2 = m_d1

        out_blocks = {
            "MODSEL": {"1": 0},
            "SMINPUTS": {
                "2": 1.166379e-05,
                "3": 1.184000e-01,
                "4": 9.118760e01,
                "5": 4.180000e00,
                "6": 1.731000e02,
                "7": 1.776820e00,
            },
            "EXTPAR": {
                "0": -1,
                "1": M1,
                "2": M2,
                "3": M3,
                "11": At,
                "12": Ab,
                "13": Atau,
                "23": mu,
                "26": M_A,
                "31": m_L1,
                "32": m_L2,
                "34": m_e1,
                "35": m_e2,
                "33": m_L3,
                "36": m_e3,
                "41": m_Q1,
                "42": m_Q2,
                "44": m_u1,
                "45": m_u2,
                "47": m_d1,
                "48": m_d2,
                "43": m_Q3,
                "46": m_u3,
                "49": m_d3,
                "25": tanb,
            },
            "SPHENOINPUT": {"1": -1, "2": 0, "11": 0, "12": 1.00000000e-04, "21": 0},
        }
    else:
        raise ValueError("Model variable invalid.")

    with open(f"Input_{EPISODE}.in", "w+") as f:
        for current_block in out_blocks.keys():
            xslha.write_les_houches(current_block, out_blocks[current_block], None, f)

    _ = subprocess.run(
        f"{SPHENO_PATH} Input_{EPISODE}.in Result_{EPISODE}_{trial_number}.spc".split(),
        capture_output=True,
        check=False,
    )

    mh0 = float("nan")
    if MICROMEGAS:
        omega = float("nan")
    valid = False
    constraint = np.inf

    if os.path.exists(f"Result_{EPISODE}_{trial_number}.spc"):
        result = xslha.read(f"Result_{EPISODE}_{trial_number}.spc")

        if len(result.blocks.get("SPINFO").keys()) == 2:

            mh0 = result.blocks["MASS"]["25"]
            constraint_h0 = max(0, -mh0 + M_H0_LB, mh0 - M_H0_UB)

            constraint = constraint_h0

            if MICROMEGAS:
                with open(f"MO_{EPISODE}_{trial_number}.out", "w") as out:
                    _ = subprocess.run(
                        f"{MICROMEGAS_PATH} Result_{EPISODE}_{trial_number}.spc".split(),
                        stdout=out,
                        stderr=out,
                        universal_newlines=True,
                        check=False,
                    )
                if os.path.exists(f"MO_{EPISODE}_{trial_number}.out"):
                    with open(f"MO_{EPISODE}_{trial_number}.out", "r") as out:
                        output = out.read()
                        if "Omega=" in output:
                            idx_begin = output.find("Omega=")
                            omega = float(output[idx_begin + 6 : idx_begin + 14])
                            trial.set_user_attr("omega", omega)
                            constraint_omega = max(0, -omega + OMEGA_LB, omega - OMEGA_UB)
                        else:
                            constraint_omega = np.inf

                        constraint += constraint_omega
                    os.remove(f"MO_{EPISODE}_{trial_number}.out")

            # Some specific parameters of pheno interest
            for mpid, minpar in result.blocks["MINPAR"].items():
                trial.set_user_attr(f"MINPAR_{MPID_TO_NAME[mpid]}", minpar)
            for epid, extpar in result.blocks["EXTPAR"].items():
                trial.set_user_attr(f"EXTPAR_{EPID_TO_NAME[epid]}", extpar)
            for pid, mass in result.blocks["MASS"].items():
                trial.set_user_attr(f"m_{PID_TO_NAME[pid]}", mass)
            for idx, au in result.blocks["AD"].items():
                trial.set_user_attr(f"Ad_{idx}", au)
            for idx, au in result.blocks["AE"].items():
                trial.set_user_attr(f"Ae_{idx}", au)
            for idx, au in result.blocks["AU"].items():
                trial.set_user_attr(f"Au_{idx}", au)
            for msid, mass in result.blocks["MSOFT"].items():
                trial.set_user_attr(f"msoft_{MSID_TO_NAME[msid]}", mass)
            for hmid, par in result.blocks["HMIX"].items():
                trial.set_user_attr(f"mhmix_{HMID_TO_NAME[hmid]}", par)

        valid = constraint == 0
        os.remove(f"Result_{EPISODE}_{trial_number}.spc")

    if os.path.exists(f"Input_{EPISODE}.in"):
        os.remove(f"Input_{EPISODE}.in")

    trial.set_user_attr("valid_point", valid)

    return constraint


# %%
study = optuna.create_study(sampler=SAMPLERS[SAMPLER](**SAMPLERS_CONFIG[SAMPLER]))
study.optimize(objective, n_trials=N_TRIALS, timeout=EPISODE_TIMEOUT)
study.trials_dataframe().to_csv(f"{EPISODE}_trials.csv")
